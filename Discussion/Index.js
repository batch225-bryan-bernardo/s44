/* 
    Fetch Method

    - The fetch() method in JavaScript is used to request data from a server. The request can be 
    of any type of APIs that returns data in JSON or XML.

    Sample fetching:

    fetch('https://jsonplaceholder.typicode.com/posts') - api for the get request.

*/


fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

// add post data

// Making Post Request using Fetch: Post requests can be made using fetch by giving options as given below

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),

        headers: { 'Content-Type': 'application/JSON; charset=UTF-8' }
    }).then((response) => response.json()).then((data) => {
        console.log(data);
        alert('Successfully added')

        document.querySelector('#txt-title').value = null;
        document.querySelector("#txt-body").value = null;
    })

})

// Show posts

const showPosts = (posts) => {

    let postEntries = '';

    posts.forEach((post) => {

        postEntries +=
            `<div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
             </div>
            `

    });

    document.querySelector(`#div-post-entries`).innerHTML = postEntries;
}

// Edit post

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;

    // removeAttribute method removes the "disabled" attribute from the selected element
    document.querySelector(`#btn-submit-update`).removeAttribute(`disabled`)
}

// Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault()
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: { 'Content-Type': 'application/json; charset=UTF-8' }
    }).then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert('Successfully Updated')

            document.querySelector('#txt-edit-id').value = null;
            document.querySelector('#txt-edit-title').value = null;
            document.querySelector('#txt-edit-body').value = null;

            // This code could be used to disable a submit button on a form, preventing the user from interacting with the button and submitting the form.

            // When a submit button is disabled, it appears grayed out and cannot be click.

            document.querySelector('#btn-submit-update').setAttribute('disabled', true);


        });
})

// Activity [Feb 3, 2023]

// Delete post

const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
        {
            method: 'DELETE'
        })
    document.querySelector(`#post-${id}`).remove();
}

